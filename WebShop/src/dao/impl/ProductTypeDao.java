package dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import dao.DataAccessObject;
import model.ProductType;

public class ProductTypeDao implements DataAccessObject<ProductType> {

	private Map<Integer, ProductType> productTypeMap;
	private int idIndex = 0;
	
	public ProductTypeDao(Map<Integer, ProductType> productTypeMap) {
		super();
		this.productTypeMap = productTypeMap;
	}

	@Override
	public void create(ProductType pt) {
		productTypeMap.put(idIndex++, pt);
	}

	@Override
	public void remove(Integer id) {
		productTypeMap.remove(id);
	}

	@Override
	public List<ProductType> list() {
		return new ArrayList<>(productTypeMap.values());
	}

}
