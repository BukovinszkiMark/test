package model;

import java.util.ArrayList;

public class Cart {

	private ArrayList<Product> list = new ArrayList<Product>();

	public Cart() {

	}

	public void add(Product product) {
		list.add(product);
	}
	
	@Override
	public String toString() {
		return "Cart [list=" + list + "]";
	}
	
}
