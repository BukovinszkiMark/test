package model;

public class Product {
	
	private ProductType productType;
	private int IDNumber;
	
	public Product(ProductType productType, int iDNumber) {
		super();
		this.productType = productType;
		IDNumber = iDNumber;
	}
	
	
	
	public ProductType getProductType() {
		return productType;
	}



	public int getIDNumber() {
		return IDNumber;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + IDNumber;
		result = prime * result + ((productType == null) ? 0 : productType.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (IDNumber != other.IDNumber)
			return false;
		if (productType == null) {
			if (other.productType != null)
				return false;
		} else if (!productType.equals(other.productType))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Product [productType=" + productType + ", IDNumber=" + IDNumber + "]";
	}

	
	
}
